const assert = require('assert');
const Beer = require('../../src/models/beer.model');
const Order = require('../../src/models/order.model');
const User = require('../../src/models/user.model')
const logger = require('../../src/config/dev').logger;

describe('Order unit tests', () => {
    let u;
    let b;
    let o;
    beforeEach(done => {
        u = new User({
            username: 'test',
            password: 'test1234!',
            emailAddress: 'test@gmail.com',
            profileImg: 'test',
            country: 'Netherlands',
            birthDate: new Date(2000, 2, 2),
            address: 'Testlaan 123',
            postalCode: '2222 AA',
            roles: [0]
        });
        u.save()
            .then(() => User.findOne({username: 'test'}))
            .then((ures) => {
                u = ures;
                b = new Beer({
                    name: 'Test',
                    description: 'test bier',
                    price: 1.50,
                    alcoholPerc: 0.063,
                    content: 0.33,
                    brand: 'Test',
                    imageUrl: 'test',
                    user: u
                });
                b.save()
                    .then(() => Beer.findOne({name: 'Test'}))
                    .then((bres) => {
                        b = bres;
                        o = new Order({
                            user: u,
                            orderlines: [{
                                quantity: 1,
                                total: 1.50,
                                beer: b
                            }],
                            submitDate: Date.now()
                        });
                        o.save()
                            .then(() => {
                                done();
                            })
                    });
            });
    });

    it('Should create a new Order', done => {
        let order = new Order({
            user: u,
            orderlines: [{
                quantity: 1,
                total: 1.50,
                beer: b
            }],
            submitDate: Date.now()
        });
        order.save()
            .then(() => Order.findOne({ user: u._id }))
            .then(res => {
                assert(res.orderlines.length === 1);
                assert(res.orderlines[0].total === 1.50)
                done();
            })
    })
})
