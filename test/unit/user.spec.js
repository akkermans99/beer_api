const assert = require('assert');
const User = require('../../src/models/user.model');
const logger = require('../../src/config/dev').logger;

describe('User unit tests', () => {
    let username = 'test';
    let emailAddress = 'test@gmail.com';
    let u;

    beforeEach(done => {
        u = new User({
            username: username,
            password: 'test1234!',
            emailAddress: emailAddress,
            profileImg: 'test',
            country: 'Netherlands',
            birthDate: new Date(2000, 2, 2),
            address: 'Testlaan 123',
            postalCode: '2222 AA',
            roles: [0]
        });
        u.save().then(() => {
            done();
        });
    });

    it('Should create a new user', done => {
        let user = new User({
            username: 'test123',
            password: 'test1234!',
            emailAddress: 'tester@gmail.com',
            profileImg: 'test',
            country: 'Netherlands',
            birthDate: new Date(2000, 2, 2),
            address: 'Testlaan 123',
            postalCode: '2222 AA',
            roles: [0]
        });

        user.save()
            .then(() => User.findOne({ username: 'test123' }))
            .then(usr => {
                assert(usr.profileImg === 'test');
                assert(usr.country === 'Netherlands');
                done();
            })
    });

    it('Should not create a user if password length is less then 8', done => {
        let user = new User({
            username: 'test123',
            password: 'test',
            emailAddress: 'tester@gmail.com',
            profileImg: 'test',
            country: 'Netherlands',
            birthDate: new Date(2000, 2, 2),
            address: 'Testlaan 123',
            postalCode: '2222 AA',
            roles: [0]
        });

        user.save().catch(err => {
            assert(err.name, 'ValidationError');
            done();
        })
    });

    it('Should not create a user if the emailAddress is not a correct one', done => {
        let user = new User({
            username: 'test123',
            password: 'test1234!',
            emailAddress: 'test',
            profileImg: 'test',
            country: 'Netherlands',
            birthDate: new Date(2000, 2, 2),
            address: 'Testlaan 123',
            postalCode: '2222 AA',
            roles: [0]
        });

        user.save()
            .catch(err => {
                assert(err.name, 'ValidationError');
                done();
            })
    });

    it('Should encrypt the password', done => {
        let user = new User({
            username: 'test123',
            password: 'test1234!',
            emailAddress: 'tester@gmail.com',
            profileImg: 'test',
            country: 'Netherlands',
            birthDate: new Date(2000, 2, 2),
            address: 'Testlaan 123',
            postalCode: '2222 AA',
            roles: [0]
        });

        user.save()
            .then(() => User.findOne({ username: 'test123' }))
            .then(usr => {
                assert(usr.password !== 'test1234!' );
                done();
            });
    });

    it('Should not create an user when the username is shorter then 3 chars', done => {
        let user = new User({
            username: 'tt',
            password: 'test1234!',
            emailAddress: 'tester@gmail.com',
            profileImg: 'test',
            country: 'Netherlands',
            birthDate: new Date(2000, 2, 2),
            address: 'Testlaan 123',
            postalCode: '2222 AA',
            roles: [0]
        });

        user.save()
            .catch(err => {
                assert(err.name, 'ValidationError');
                done();
            });
    });
});
