const assert = require('assert');
const Beer = require('../../src/models/beer.model');
const Comment = require('../../src/models/comment.model');
const User = require('../../src/models/user.model')
const logger = require('../../src/config/dev').logger;

describe('Comment unit tests', () => {
    let u;
    let b;
    let c;
    beforeEach(done => {
        u = new User({
            username: 'test',
            password: 'test1234!',
            emailAddress: 'test@gmail.com',
            profileImg: 'test',
            country: 'Netherlands',
            birthDate: new Date(2000, 2, 2),
            address: 'Testlaan 123',
            postalCode: '2222 AA',
            roles: [0]
        });
        u.save()
            .then(() => User.findOne({username: 'test'}))
            .then((ures) => {
                u = ures;
                b = new Beer({
                    name: 'Test',
                    description: 'test bier',
                    price: 1.50,
                    alcoholPerc: 0.063,
                    content: 0.33,
                    brand: 'Test',
                    imageUrl: 'test',
                    user: u
                });
                b.save()
                    .then(() => Beer.findOne({name: 'Test'}))
                    .then((bres) => {
                        b = bres;
                        c = new Comment({
                            content: 'testing',
                            rating: 3,
                            author: u,
                            beer: b,
                            submitDate: Date.now()
                        });
                        c.save()
                            .then(() => {
                                done();
                            })
                    });
            });
    });

    it('Should create a new Comment', done => {
        let comment = new Comment({
            content: 'test',
            rating: 5,
            beer: b,
            author: u,
            submitDate: Date.now()
        });
        comment.save()
            .then(() => Comment.findOne({ rating: 5 }))
            .then(res => {
                assert(res.content === 'test');
                done();
            })
    })
})
