const assert = require('assert');
const Beer = require('../../src/models/beer.model');
const User = require('../../src/models/user.model');
const logger = require('../../src/config/dev').logger;

describe('Beer unit tests', () => {
    let b;
    let u;
    beforeEach(done => {
        u = new User({
            username: 'test',
            password: 'test1234!',
            emailAddress: 'test@gmail.com',
            profileImg: 'test',
            country: 'Netherlands',
            birthDate: new Date(2000, 2, 2),
            address: 'Testlaan 123',
            postalCode: '2222 AA',
            roles: [0]
        });
        u.save()
            .then(() => User.findOne({username: 'test'}))
            .then((res) => {
                u = res;
                b = new Beer({
                    name: 'Test',
                    description: 'test bier',
                    price: 1.50,
                    alcoholPerc: 0.063,
                    content: 0.33,
                    brand: 'Test',
                    imageUrl: 'test',
                    user: res._id
                });
                b.save().then(() => {
                    done();
                });
        });

    });

    it('Should create a new beer', done => {
        let beer = new Beer({
            name: 'Tester',
            description: 'test bier',
            price: 1.50,
            alcoholPerc: 0.063,
            content: 0.33,
            brand: 'Test',
            imageUrl: 'test'
        });
        beer.save()
            .then(() => Beer.findOne({ name: 'Tester' }))
            .then(res => {
                assert(res.brand === 'Test');
                assert(res.price === 1.50);
                done();
            })
    })
})
