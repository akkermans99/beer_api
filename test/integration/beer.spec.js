const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../src/index');
const expext = chai.expect;
const logger = require('../../src/config/dev').logger;

chai.should();
chai.use(chaiHttp);

const username = 'tester88';
const password = 'tester88';
const beerUrl = '/api/beers';

let token;

describe('API endpoints beer', () => {
    let user = {
        username: username,
        password: password,
        profileImg: 'test',
        emailAddress: 'tester88@gmail.com',
        country: 'Netherlands',
        postalCode: '1111 AB',
        address: 'Testlaan 88',
        birthDate: new Date(1988, 1, 1)
    };

    let beer = {
        name: 'Test',
        description: 'test bier',
        price: 1.50,
        alcoholPerc: 0.063,
        content: 0.33,
        brand: 'Test',
        imageUrl: 'test',
        specie: ['Test']
    }

    beforeEach(done => {
        chai
            .request(server)
            .post('/api/users')
            .set('Content-Type', 'application/json')
            .send(user)
            .end((err, res) => {
                if (err) {
                    done(err);
                }
                user = res.body.result;
                user.password = password;
                chai
                    .request(server)
                    .post('/api/users/login')
                    .set('Content-Type', 'application/json')
                    .send(user)
                    .end((err1, resp) => {
                        if (err1) {
                            done(err1)
                        }
                        token = resp.body.token;
                        beer.user = user;
                        chai
                            .request(server)
                            .post('/api/beers')
                            .set('Content-Type', 'application/json')
                            .set('Authorization', 'Bearer ' + token)
                            .send(beer)
                            .end((error, bres) => {
                                if ( error ) {
                                    done(error);
                                }
                                beer = bres.body.result;
                                done();
                            })
                    })
            });
    });

    it('should create a new beer', done => {
        let tbeer = {
            name: 'temp',
            description: 'test bier',
            price: 1.50,
            alcoholPerc: 0.063,
            content: 0.33,
            brand: 'Test',
            imageUrl: 'test',
            user: user
        }
        chai
            .request(server)
            .post('/api/beers')
            .set('Content-Type', 'application/json')
            .set('Authorization', 'Bearer ' + token)
            .send(tbeer)
            .end((err, res) => {
                if ( err ) {
                    done(err);
                }
                expext(res).to.exist;
                expext(res).to.have.status(200)
                expext(res.body).to.have.property('result')
                    .to.have.property('name')
                    .to.equal('temp');
                done();
            })
    });

    it('Should get all the beers', done => {
       chai
           .request(server)
           .get(beerUrl)
           .end((err, res) => {
               if (err) {
                   done(err)
               }
               expext(res).to.exist;
               expext(res).to.have.status(200)
               expext(res.body).to.have.property('result')
                   .to.be.an('array');
               expext(res.body.result[0]).to.have.property('name')
                   .to.equal('Test');
               done();
           })
    });

    it('Should get all the beers by specie', done => {
        chai
            .request(server)
            .get(beerUrl + '/species/Test')
            .end((err, res) => {
                if (err) {
                    done(err)
                }
                logger.info(res.body)
                expext(res).to.exist;
                expext(res).to.have.status(200)
                expext(res.body).to.have.property('result')
                    .to.be.an('array');
                expext(res.body.result[0]).to.have.property('name')
                    .to.equal('Test');
                done();
            })
    });

    it('Should get a beer by id', done => {
        chai
            .request(server)
            .get(beerUrl + '/' + beer._id)
            .end((err, res) => {
                if (err) {
                    done(err)
                }
                expext(res).to.exist;
                expext(res).to.have.status(200)
                expext(res.body).to.have.property('result')
                    .to.be.an('array');
                expext(res.body.result[0]).to.have.property('name')
                    .to.equal('Test');
                done();
            })
    });

    it('Should get all the beers that belong on the homepage', done => {
        chai
            .request(server)
            .get(beerUrl + '/home/dashboard')
            .end((err, res) => {
                if (err) {
                    done(err)
                }
                logger.info(res.body);
                expext(res).to.exist;
                expext(res).to.have.status(200)
                expext(res.body).to.have.property('result')
                    .to.have.property('topRated')
                    .to.be.an('array');
                expext(res.body).to.have.property('result')
                    .to.have.property('popular')
                    .to.be.an('array');
                expext(res.body.result.topRated[0]).to.have.property('name')
                    .to.equal('Test');
                expext(res.body.result.popular[0]).to.have.property('name')
                    .to.equal('Test');
                done();
            })
    });

    it('Should get a beer by name', done => {
        chai
            .request(server)
            .get(beerUrl + '/name/' + beer.name)
            .end((err, res) => {
                if (err) {
                    done(err)
                }
                expext(res).to.exist;
                expext(res).to.have.status(200)
                expext(res.body).to.have.property('result')
                    .to.have.property('name')
                    .to.equal('Test');
                done();
            })
    });

    it('Should get all the beers created by an user', done => {
        chai
            .request(server)
            .get(beerUrl + '/user/' + user.username)
            .end((err, res) => {
                if (err) {
                    done(err)
                }
                expext(res).to.exist;
                expext(res).to.have.status(200)
                expext(res.body).to.have.property('result')
                    .to.be.an('array');
                expext(res.body.result[0]).to.have.property('name')
                    .to.equal('Test');
                done();
            })
    });

    it('Should create a comment and add it to the beer', done => {
        let comment = {
            content: 'Test comment',
            rating: 4
        };
        chai
            .request(server)
            .post(beerUrl + '/' + beer._id + '/comment')
            .set('Content-Type', 'application/json')
            .set('Authorization', 'Bearer ' + token)
            .send(comment)
            .end((err, res) => {
                if (err) {
                    done(err)
                }
                logger.info(res.body)
                expext(res).to.exist;
                expext(res).to.have.status(200)
                expext(res.body).to.have.property('result')
                    .to.be.an('object');
                expext(res.body.result).to.have.property('name')
                    .to.equal('Test');
                expext(res.body.result).to.have.property('comments')
                    .to.be.an('array')
                    .to.have.length(1);
                done();
            })
    });
});
