const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../src/index');
const expext = chai.expect;
const logger = require('../../src/config/dev').logger;

chai.should();
chai.use(chaiHttp);

const username = 'tester88';
const password = 'tester88';


let token;

describe('API endpoints user', () => {
    const user = {
        username: username,
        password: password,
        profileImg: 'test',
        emailAddress: 'tester88@gmail.com',
        country: 'Netherlands',
        postalCode: '1111 AB',
        address: 'Testlaan 88',
        birthDate: new Date(1988, 1, 1)
    };

    beforeEach(done => {
        chai
            .request(server)
            .post('/api/users')
            .set('Content-Type', 'application/json')
            .send(user)
            .end((err, res) => {
                if ( err ) {
                    done(err);
                }
                chai
                    .request(server)
                    .post('/api/users/login')
                    .set('Content-Type', 'application/json')
                    .send(user)
                    .end((err, res) => {
                        if ( err ) {
                            done(err)
                        }
                        token = res.body.token;
                        done();
                    })
            });
    })

    it('Should create a new user', done => {
        let tu = {
            username: 'testing',
            password: password,
            profileImg: 'test',
            emailAddress: 'test@outlook.com',
            country: 'Netherlands',
            postalCode: '1111 AB',
            address: 'Testlaan 88',
            birthDate: new Date(1988, 1, 1)
        };
        chai
            .request(server)
            .post('/api/users')
            .set('Content-Type', 'application/json')
            .send(tu)
            .end((err, res) => {
              if ( err ) {
                  done(err);
              }
              expext(res).to.exist;
              expext(res).to.have.status(200);
              expext(res.body).to.exist;
              expext(res.body)
                  .to.have.property('result')
                  .to.be.an('object');
              done()
            });
    });

    it('should login an existing user', done => {
        chai
            .request(server)
            .post('/api/users/login')
            .set('Content-Type', 'application/json')
            .send(user)
            .end((err, res) => {
                if ( err ) {
                    done(err)
                }
                expext(res).to.exist;
                expext(res).to.have.status(200);
                expext(res.body).to.exist;
                expext(res.body)
                    .to.have.property('token');
                token = res.body.token;
                done();
            })
    });

    it('should get all the users', done => {
        chai
            .request(server)
            .get('/api/users')
            .set('Authorization', 'Bearer ' + token)
            .end((err, res) => {
                if ( err ) {
                    done(err);
                }
                expext(res).to.exist;
                expext(res).to.have.status(200);
                expext(res.body).to.have.property('result');
                done()
            })
    });

    it('should change the address from the user with username: tester88', done => {
        chai
            .request(server)
            .put('/api/users/tester88')
            .set('Content-Type', 'application/json')
            .set('Authorization', 'Bearer ' + token)
            .send({
                address: 'ChangedAddress 88'
            })
            .end((err, res) => {
                if ( err ) {
                    done(err);
                }
                expext(res).to.exist;
                expext(res).to.have.status(200);
                expext(res.body).to.have.property('result')
                    .to.have.property('address')
                    .to.equal('ChangedAddress 88');
                done();
            })
    });

    it('should get all the orders from an user', done => {
        chai
            .request(server)
            .get('/api/users/tester88/orders')
            .set('Authorization', 'Bearer ' + token)
            .end((err, res) => {
                if ( err ) {
                    done(err);
                }
                expext(res).to.exist;
                expext(res).to.have.status(200);
                expext(res.body).to.have.property('result')
                    .to.be.an('array');
                done()
            })
    });

})
