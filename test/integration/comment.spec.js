const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../src/index');
const expext = chai.expect;
const logger = require('../../src/config/dev').logger;

chai.should();
chai.use(chaiHttp);

const username = 'tester88';
const password = 'tester88';
const beerUrl = '/api/beers';

let token;

describe('API endpoints comments', () => {
    let user = {
        username: username,
        password: password,
        profileImg: 'test',
        emailAddress: 'tester88@gmail.com',
        country: 'Netherlands',
        postalCode: '1111 AB',
        address: 'Testlaan 88',
        birthDate: new Date(1988, 1, 1)
    };

    let beer = {
        name: 'Test',
        description: 'test bier',
        price: 1.50,
        alcoholPerc: 0.063,
        content: 0.33,
        brand: 'Test',
        imageUrl: 'test',
        specie: ['Test']
    }

    let comment = {
        content: 'test',
        rating: 4
    }

    beforeEach(done => {
        chai
            .request(server)
            .post('/api/users')
            .set('Content-Type', 'application/json')
            .send(user)
            .end((err, res) => {
                if (err) {
                    done(err);
                }
                user = res.body.result;
                user.password = password;
                chai
                    .request(server)
                    .post('/api/users/login')
                    .set('Content-Type', 'application/json')
                    .send(user)
                    .end((err1, resp) => {
                        if (err1) {
                            done(err1)
                        }
                        token = resp.body.token;
                        beer.user = user;
                        chai
                            .request(server)
                            .post('/api/beers')
                            .set('Content-Type', 'application/json')
                            .set('Authorization', 'Bearer ' + token)
                            .send(beer)
                            .end((error, bres) => {
                                if (error) {
                                    done(error);
                                }
                                beer = bres.body.result;

                                chai
                                    .request(server)
                                    .post('/api/beers/' + beer._id + '/comment')
                                    .set('Content-Type', 'application/json')
                                    .set('Authorization', 'Bearer ' + token)
                                    .send(comment)
                                    .end(((err2, res1) => {
                                        if (err2) {
                                            done(err2);
                                        }
                                        comment = res1.body.result.comments[0];
                                        done();
                                    }))
                            })
                    })
            });
    });

    it('Should create a new Comment', done => {
        let c = {
            content: 'test',
            rating: 4,
            beer: beer
        }

        chai
            .request(server)
            .post('/api/comments')
            .set('Content-Type', 'application/json')
            .set('Authorization', 'Bearer ' + token)
            .send(c)
            .end((err, res) => {
                if (err) {
                    done(err);
                }
                expext(res).to.exist;
                expext(res).to.have.status(200);
                expext(res.body).to.have.property('result')
                    .to.have.property('content')
                    .to.equal('test');
                done();
            });
    });

});
