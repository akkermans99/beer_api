const mongoose = require('mongoose');
const devconfig = require('../../src/config/dev');
const logger = devconfig.logger;
const testURI = devconfig.Test_DB_URI;

mongoose.Promise = global.Promise;

before(done => {
    mongoose.connect(
        'mongodb+srv://test:pzc7020DDwkZH8mg@cluster0-trebb.azure.mongodb.net/test?retryWrites=true&w=majority',
        {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: true,
            keepAlive: true,
            keepAliveInitialDelay: 300000
    }).then(() => {
        logger.info('Connected to database');
        done();
    })
        .catch(err => {
            logger.error('Database connection failed');
            done(err);
        });
});

beforeEach(done => {
    const { users, beers, comments, orders } = mongoose.connection.collections;
    if ( users ) {
        users.drop(() => {
            beers.drop(() => {
                if ( comments && orders ) {
                    comments.drop(() => {
                        if ( orders ) {
                            orders.drop(() => {
                                done();
                            })
                        }
                    });
                }
                else if ( orders ) {
                    orders.drop(() => {
                        done();
                    })
                }
                else if ( comments ) {
                    comments.drop(() => {
                        done()
                    })
                }
                else {
                    done();
                }
            });
        });
    } else {
        done();
    }

});
