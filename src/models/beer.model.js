const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const beerSchema = new Schema({
    name: {
        type: String,
        required: [true, "Beer name is required"],
        unique: [true, "This name already exists"]
    },
    description: {
        type: String
    },
    price: {
        type: Number
    },
    alcoholPerc: {
        type: Number
    },
    content: {
        type: Number
    },
    brand: {
        type: String
    },
    imageUrl: {
        type: String
    },
    comments: [{
        type: Schema.Types.ObjectId,
        ref: "Comment"
    }],
    specie: [String],
    image: {
        data: Buffer,
        contentType: String
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
});

beerSchema.virtual('avgCommentRating').get(function () {
    let total = 0;
    let count = this.comments.length;
    for ( let i = 0; i < count; i++ ) {
        total += this.comments[i].rating;
    }
    if ( count === 0 ) {
        return 0;
    }
    return Math.round((total / count) * 100) / 100;
});

beerSchema.set('toObject', {virtuals: true});
beerSchema.set('toJSON', {virtuals: true});

module.exports = mongoose.model("Beer", beerSchema);
