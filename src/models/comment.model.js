const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const commentSchema = new Schema({
    content: {
        type: String,
        required: [true, "Content is required by a comment"]
    },
    beer: {
        type: Schema.Types.ObjectId,
        ref: "Beer",
        required: [true, "A comment must be linked to a beer"]
    },
    rating: {
        type: Number,
        default: 0
    },
    author: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: [true, "A user must have written this comment"]
    },
    comments: [{
        type: Schema.Types.ObjectId,
        ref: 'Comment',
        default: []
    }],
    submitDate: {
        type: Schema.Types.Date,
        required: [true, "Submit date is required"]
    },
    lastUpdateDate: {
        type: Schema.Types.Date,
        default: null
    }
});

module.exports = mongoose.model('Comment', commentSchema);
