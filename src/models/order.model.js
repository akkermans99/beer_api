const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const orderSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: [true, "User is required"]
    },
    orderlines: [{}],
    submitDate: {
        type: Date,
        default: new Date()
    }
});

module.exports = mongoose.model('Order', orderSchema);
