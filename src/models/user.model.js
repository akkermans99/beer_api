const mongoose = require('mongoose');
const bcrypt = require("bcrypt");
const Schema = mongoose.Schema;

const emailRe = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const userSchema = new Schema({
    username: {
        type: String,
        required: [true, "Username is required"],
        unique: [true, "Username must be unique"],
        validate: {
            validator: username => username.length > 2,
            message: "Username must be longer then 2 char"
        }
    },
    password: {
        type: String,
        validate: {
            validator: password => password.length > 7,
            message: "Password must be at least eight characters!"
        },
        required: [true, "Password is required!"]
    },
    profileImg: {
        type: String
    },
    emailAddress: {
        type: String,
        unique: [true, "Email address is already been used!"],
        validate: {
            validator: email => emailRe.test(String(email).toLowerCase()),
            message: "Not a valid email address!"
        },
        required: [true, "Email address is required!"]
    },
    birthDate: {
        type: Date,
        required: [true, "Birth date is required!"]
    },
    address: {
        type: String,
        required: [true, "Address is required"]
    },
    postalCode: {
        type: String,
        required: [true, "Postal code is required"]
    },
    country: {
        type: String,
        required: [true, "Country is required"]
    },
    roles: [Number]
});

userSchema.pre("save", function(next) {
    const user = this;

    bcrypt.genSalt(10, function(err, salt) {
        if (err) {
            return next(err);
        }

        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) {
                return next(err);
            }

            user.password = hash;
            next();
        });
    });
});

//Every user have acces to this methods
userSchema.methods.comparePassword = function(candidatePassword, callback) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) {
            return callback(err);
        }

        callback(null, isMatch);
    });
};

module.exports = mongoose.model("User", userSchema);
