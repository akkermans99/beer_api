const Beer = require('../models/beer.model');

module.exports = {
    DB_URI: process.env.DB_URI || "mongodb+srv://lgaakker:Ew7rPKJ4Vm4VQZgF@beer-wutrl.gcp.mongodb.net/BeerDB?retryWrites=true&w=majority",
    Test_DB_URI: 'mongodb+srv://test:pzc7020DDwkZH8mg@cluster0-trebb.azure.mongodb.net/test?retryWrites=true&w=majority',
    logger: require("tracer").colorConsole({
        format: [
            "{{timestamp}} <{{title}}> {{message}} (in {{file}}:{{line}})", //default format
            {
                error: "{{timestamp}} <{{title}}> {{message}} (in {{file}}:{{line}})" // error format
            }
        ],
        dateformat: "HH:MM:ss.L",
        preprocess: function (data) {
            data.title = data.title.toUpperCase();
        },
        level: process.env.LOG_LEVEL || "trace"
    }),
    mockdata: () => {
        const cornet = new Beer({
            name: "Cornet",
            description: "Sterk blond bier gebrouwen ter ere van ridder Salamon de Maldeghem, die leefde in de 11e eeuw. Goudkleurig met forse witte schuimkraag. De subtiele vanille-tonen zijn afkomstig van de verse eiken chips.",
            content: 0.33,
            alcoholPerc: 0.085,
            brand: "Palm",
            price: 1.99,
            imageUrl: "https://www.beerwulf.com/globalassets/catalog/beerwulf/beers/de_hoorn_-_cornet_opt.png?h=500&rev=899257661",
            comments: [],
            specie: ["Blond"]
        });

        cornet.save();

        const leffe_blond = new Beer({
            name: "Leffe blond",
            brand: "Leffe",
            specie: ["Blond"],
            description: "Donker goudgeel tot oranjekleurig, helder, met een stevig volle witte schuimkraag. Zeer toegankelijke fruitige Blond met een licht bittertje. Leffe Blond is een erkend abdijbier.",
            content: 0.30,
            price: 1.69,
            alcoholPerc: 0.066,
            imageUrl: "https://www.beerwulf.com/globalassets/catalog/beerwulf/beers/blond/leffe-blond2.png?h=500&rev=1554326465",
            comments: []
        });

        leffe_blond.save();

        const leffe_tripel = new Beer({
            name: "Leffe tripel",
            brand: "Leffe",
            specie: ["Tripel"],
            description: "Goud tot oranjekleurige met flinke, licht gekleurde schuimkraag. Smaak is zoet, alcoholisch, fruitig met kruidigheid van koriander.",
            content: 0.33,
            price: 2.29,
            alcoholPerc: 0.085,
            imageUrl: "https://www.beerwulf.com/globalassets/catalog/beerwulf/beers/belgium/leffe-triple.png?h=500&rev=1073467326",
            comments: []
        });

        leffe_tripel.save();

        const laChoeffe = new Beer({
            name: "La Chouffe Blonde D'ardenne",
            brand: "La Chouffe",
            specie: ["Blond"],
            description: "Goudkleurig bier, licht nevelig met een robuuste schuimkraag. Kruidige geur met veel citrusfruit en koriander. Ook in de smaak veel fris fruit en koriander.",
            content: 0.33,
            price: 1.99,
            alcoholPerc: 0.080,
            imageUrl: "https://www.beerwulf.com/globalassets/catalog/beerwulf/beers/la-chouffe-blonde-d-ardenne_opt.png?h=500&rev=899257661",
            comments: []
        });

        laChoeffe.save();

        const delerium = new Beer({
            name: "Delirium Tremens",
            brand: "L. Huyghe",
            specie: ["Blond", "Tripel"],
            description: "Goudkleurig, helder met een volle witte schuimkraag. Koolzuurrijk, klassiek Belgisch bier met veel alcohol en met fruitige gist en een zoete afdronk.",
            content: 0.33,
            price: 2.09,
            alcoholPerc: 0.085,
            imageUrl: "https://www.beerwulf.com/globalassets/catalog/beerwulf/beers/blond/delirium_tremens_2.png?h=500&rev=204392068",
            comments: []
        });

        const kwak = new Beer({
            name: "Pauwel kwak",
            brand: "Bosteels",
            specie: ["Amber", "Tripel"],
            description: "Koperkleurig, romige volle schuimkraag. Dit bier is wereldberoemd vanwege het zogenaamde Koetsiersglas. Warme zoete aroma’s van mout, karamel, toffee en een subtiel kruidig bittere afdronk.",
            content: 0.33,
            price: 1.99,
            alcoholPerc: 0.084,
            imageUrl: "https://www.beerwulf.com/globalassets/catalog/beerwulf/beers/pale-ale/pauwel---kwak.png?h=500&rev=1966168829",
            comments: []
        });

        const afflichemBlond = new Beer({
            name: "Affligem Blond",
            brand: "Affligem",
            specie: ["Blond"],
            description: "Goud tot oranjekleurige met flinke, licht gekleurde schuimkraag. Smaak is zoet, alcoholisch, fruitig met kruidigheid van koriander.",
            content: 0.30,
            price: 1.29,
            alcoholPerc: 0.068,
            imageUrl: "https://www.beerwulf.com/globalassets/catalog/beerwulf/beers/blond/affligem_blond_2.png?h=500&rev=204392068",
            comments: []
        });

        afflichemBlond.save();
        kwak.save();
        delerium.save();

        const tricky_tripel = new Beer({
            name: "Gebrouwen Door Vrouwen Tricky Tripel",
            brand: "Gebrouwen Door Vrouwen",
            specie: ["Tripel"],
            description: "Deze Tripel heeft een redelijk zoet voorkomen doordat er veel gerstemout gebruikt is tijdens het brouwen. Hints van abrikoos en gele pruim en een beetje droge afdronk maken dit bier af.",
            content: 0.33,
            price: 2.59,
            alcoholPerc: 0.078,
            imageUrl: "https://www.beerwulf.com/globalassets/gebrouwen-door-vrouwen-tricky-tripel.33_1_1?h=500&rev=2077567413"
        });

        tricky_tripel.save();

    }
};
