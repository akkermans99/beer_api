const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const logger = require("./config/dev").logger;
const PORT = process.env.PORT || 4747;
const mongoose = require("mongoose");
const db = require("./config/dev").DB_URI;
const multer = require('multer');

const app = express();

const beerRoutes = require('./routes/beer.routes');
const userRoutes = require('./routes/user.routes');
const commentRoutes = require('./routes/comment.routes');
const orderRoutes = require('./routes/order.routes');

mongoose
    .connect(db, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true
    })
    .then(() => logger.info("DB Connected!"))
    .catch(err => logger.error(err));



app.use(bodyParser.json());
app.use(morgan("dev"));
app.use(cors());

beerRoutes(app);
userRoutes(app);
commentRoutes(app);
orderRoutes(app);

//require('./config/dev').mockdata();

// Handle endpoint not found.
app.all("*", function(req, res, next) {
    logger.error("Endpoint not found.");
    var errorObject = {
        message: "Endpoint does not exist!",
        code: 404,
        date: new Date()
    };
    next(errorObject);
});

// Error handler
app.use((error, req, res, next) => {
    res.status(error.code).json(error);
});

// Determine on wich port the server is listening to
app.listen(PORT, function() {
    logger.debug("Server is running on port: " + PORT);
});

/*app.use(multer({dest: './uploads',
    rename: function (fieldname, filename) {
        return filename
    }
}));*/

module.exports = app;
