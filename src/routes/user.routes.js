const express = require('express');
const router = express.Router();
const auth = require('../services/authentication');
const userController = require('../controllers/user.controller');

module.exports = app => {
    app.get('/api/users', auth.validateToken, userController.getAllUsers);

    app.post('/api/users', userController.createUser);
    app.post('/api/users/login', userController.loginUser);

    app.put('/api/users/:username', auth.validateToken, userController.updateUser);
    app.delete('/api/users/:username', auth.validateToken, userController.deleteUser);

    app.get('/api/users/:username/orders', auth.validateToken, userController.getAllOrdersFromUser);
    app.get('/api/admins', auth.validateToken, userController.getAllAdmins)
    
};
