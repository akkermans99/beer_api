const express = require('express');
const router = express.Router();
const auth = require('../services/authentication');
const orderController = require('../controllers/order.controller');

module.exports = app => {
    app.get('/api/orders', auth.validateToken, orderController.getAllOrders);
    app.get('/api/orders/:id', auth.validateToken, orderController.getOrderById);
    app.get('/api/orders/user/:username', auth.validateToken, orderController.getOrdersByUsername);

    app.post('/api/orders', auth.validateToken, orderController.createOrder);

    app.put('/api/orders/:id', auth.validateToken, orderController.updateOrder);
    app.delete('/api/orders/:id', auth.validateToken, orderController.deleteOrder);
};
