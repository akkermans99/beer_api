const express = require('express');
const router = express.Router();
const auth = require('../services/authentication');
const commentController = require('../controllers/comment.controller');


module.exports = app => {
    app.get('/api/comments', commentController.getAllComments);
    app.get('/api/comments/:id', commentController.getCommentById);

    app.post('/api/comments', auth.validateToken, commentController.postComment);

    app.put('/api/comments/:id', auth.validateToken, commentController.updateComment);
};
