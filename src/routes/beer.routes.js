const express = require('express');
const router = express.Router();
const auth = require('../services/authentication');
const beerController = require('../controllers/beer.controller');


module.exports = app => {
    app.get('/api/beers', beerController.getAllBeers);
    app.get('/api/beers/species/:specie', beerController.getAllBeersFromSpecie);
    app.get('/api/beers/:id', beerController.getBeerById);
    app.get('/api/beers/home/dashboard', beerController.getDashBoardBeers);
    app.get('/api/beers/name/:name', beerController.getBeerByName);
    app.get('/api/beers/user/:username', beerController.getBeersFromUser)

    app.post('/api/beers', auth.validateToken, beerController.postBeer);
    app.post('/api/beers/:id/comment', auth.validateToken, beerController.postCommentToBeer);

    app.put('/api/beers/:id', auth.validateToken, beerController.updateBeerById);

    app.delete('/api/beers/:id', auth.validateToken, beerController.deleteBeerById);
    app.delete('/api/beers/:id/comment/:commentId', beerController.deleteCommentFromBeer);
};
