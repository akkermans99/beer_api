const mongoose = require('mongoose');
const Beer = require('../models/beer.model');
const Comment = require('../models/comment.model');
const fs = require('fs');

module.exports = {
    getAllBeers: (req, res, next) => {
        Beer.find({})
            .populate('comments')
            .then((beers) => {
                console.log(beers);
                res.status(200).json({result: beers});
            })
            .catch(() => next({
                message: "Error get all beers",
                code: 400
            }));
    },

    postBeer: (req, res, next) => {
        const body = req.body;
        const uId = req.userId;

        let beer = new Beer({
            name: body.name,
            description: body.description,
            price: body.price,
            content: body.content,
            alcoholPerc: body.alcoholPerc,
            brand: body.brand,
            imageUrl: body.imageUrl,
            specie: body.specie,
            user: uId
        });

        /*if ( req.files.beerPhoto ) {
            beer.image.data = fs.readFileSync(req.files.beerPhoto.path);
            beer.image.contentType = 'image/png';
        }*/

        beer.save()
            .then(() => Beer.findOne({name: body.name}))
            .then((b) => {
                res.status(200).json({result: b});
            });
    },

    getBeerById: (req, res, next) => {
        const id = req.params.id;
        Beer.find({ _id: id })
            .populate('comments')
            .populate('user')
            .then((beer) => {
                res.status(200).json({result: beer});
            })
            .catch(() => next({
                message: "Error getting beer by id: " + id,
                code: 400
            }));
    },

    updateBeerById: (req, res, next) => {
        const body = req.body;
        const id = req.params.id;

        Beer.findOneAndUpdate({_id: id}, body)
            .then((beer) => {
                res.status(200).json({result: beer})
            });
    },

    deleteBeerById: (req, res, next) => {
        const id = req.params.id;

        Beer.findOneAndDelete({_id: id})
            .then((beer) => {
                res.status(200).json({result: beer});
            });
    },

    postCommentToBeer: (req, res, next) => {
        const body = req.body;
        const id = req.params.id;
        const userId = req.userId;

        let comment = new Comment({
            content: body.content,
            rating: body.rating,
            beer: id,
            author: userId,
            submitDate: new Date(),
            lastUpdateDate: new Date()
        });

        comment.save()
            .then(() => Comment.findOne({content: body.content, beer: id, author: userId}))
            .then((c) => {
                comment = c;
               Beer.findOne({_id: id})
                   .then((b) => {
                       b.comments.push(c);
                       b.save()
                           .then(() => Beer.findOne({_id: id}).populate('comments'))
                           .then((beer) => {
                               res.status(200).json({result: beer});
                           });
                   });
            });
    },

    getAllBeersFromSpecie: (req, res, next) => {
        const specie = req.params.specie;
        let specieObj = {};

        if ( specie.includes('__') ) {
            let specieArray = specie.split('__');
            specieObj.$or = [];
            for ( let i = 0; i < specieArray.length; i++ ) {
                let tObj = {
                    specie: new RegExp(".*" + specieArray[i] + ".*", "i")
                }
                specieObj.$or.push(tObj);
            }
        } else {
            specieObj.specie = new RegExp(".*" + specie + ".*", "i");
        }

        Beer.find(specieObj)
            .then((beers) => {
                res.status(200).json({result: beers});
            });
    },

    getDashBoardBeers: (req, res, next) => {
        let json = {};

        Beer.find({})
            .populate({
                path: 'comments'
            })
            .then((beers) => {
                let topRated = beers;
                topRated.sort((a, b) => {
                    return b.avgCommentRating - a.avgCommentRating;
                });
                topRated = topRated.slice(0, 5);
                json.topRated = topRated;


                let popular = beers;
                popular.sort((a, b) => {
                    return b.comments.length - a.comments.length;
                });
                popular = popular.slice(0, 5);
                json.popular = popular;
            })
            .then(() => {
                res.status(200).json({result: json});
            })


    },

    deleteCommentFromBeer: (req, res, next) => {
        const commentId = req.params.commentId;
        const beerId = req.params.id;

        Beer.findOne({_id: beerId})
            .populate('comments')
            .then((b) => {
                let i;
                b.comments.filter((c, index) => {
                    if ( c._id.toString() === commentId ) {
                        i = index;
                    }
                });
                b.comments.splice(i, 1);
                b.save()
                    .then(() => Comment.findOneAndDelete({_id: commentId}))
                    .then((c) => Beer.findOne({_id: beerId}).populate('comments'))
                    .then((beer) => {
                        res.status(200).json({result: beer});
                    });
            })
    },

    getBeerByName: (req, res, next) => {
        const name = req.params.name;
        Beer.findOne({name: name})
            .populate('comments')
            .populate('user')
            .then((b) => {
                res.status(200).json({result: b})
            });
    },

    getBeersFromUser: (req, res, next) => {
        const username = req.params.username;

        Beer.find({})
            .populate('comments')
            .populate('user')
            .then((beers) => {
                beers.filter((b) => {
                    console.log(b.user.username, username, b.user.username === username)
                    return b.user.username === username
                });
                //console.log(beers);
                res.status(200).json({result: beers.filter((b) => {
                        return b.user.username === username
                    })});
            })
            .catch((err) => {
                next({
                    code: 404,
                    message: "Something went wrong when getting the beers from uId: " + username
                })
            })
    }


};
