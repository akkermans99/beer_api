const User = require('../models/user.model');
const Order = require('../models/order.model');
const auth = require('../services/authentication');
const bcrypt = require('bcrypt');

module.exports = {
    getAllUsers: (req, res, next) => {
        User.find({})
            .then((users) => {
               res.status(200).json({result: users});
            });
    },

    createUser: (req, res, next) => {
        const body = req.body;

        User.findOne({ username: body.username }).then(foundUser => {
            if (foundUser == null) {
                let user = new User({
                    username: body.username,
                    password: body.password,
                    emailAddress: body.emailAddress,
                    birthDate: body.birthDate,
                    address: body.address,
                    postalCode: body.postalCode,
                    profileImg: body.profileImg,
                    country: body.country,
                    roles: body.roles
                });
                user.save()
                    .then(() => User.findOne({username: body.username}))
                    .then((u) => {
                        res.status(200).json({result: u});
                    });
            } else {
                next({
                    code: 400,
                    message: "Username already taken!"
                });
            }
        });


    },

    loginUser: (req, res, next) => {
        const body = req.body;

        User.findOne({ username: body.username }).then(user => {
            if (!bcrypt.compareSync(body.password, user.password)) {
                let errorObj = {
                    message: 'Login not correct',
                    code: 400
                };

                next(errorObj);
            } else {
                const token = auth.generateJWT(user);
                res.status(200).json({ token: token, user: user });
            }
        });
    },

    updateUser: (req, res, next) => {
        const body = req.body;
        const username = req.params.username;

        User.update({ username: username}, body)
            .then(() => User.findOne({username: username}))
            .then((u) => {
                res.status(200).json({result: u});
            });
    },

    getAllOrdersFromUser: (req, res, next) => {
        const username = req.params.username;
        const uid = req.userId;
        if ( req.username === username || req.username === "Admin") {
            Order.find({user: uid})
                .then((orders) => {
                    res.status(200).json({result: orders});
                });
        } else {
            next({
                code: 401,
                message: "You're not allowed to see these orders"
            })
        }
    },

    deleteUser: (req, res, next) => {
        const username = req.params.username;

        User.findOneAndDelete({username: username})
            .then((u) => {
                res.status(200).json({result: u});
            });
    },

    getAllAdmins: (req, res, next) => {
        User.find({roles: 1})
            .then((admins) => {
                res.status(200).json({result: admins})
            })
    }


};
