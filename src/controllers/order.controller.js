const Order = require('../models/order.model');

module.exports = {
    getAllOrders: (req, res, next) => {
        Order.find({})
            .then((orders) => {
                res.status(200).json({result: orders});
            });
    },

    getOrderById: (req, res, next) => {
        const id = req.params.id;

        Order.find({_id: id})
            .then((o) => {
                res.status(200).json({result: o});
            })
    },

    createOrder: (req, res, next) => {
        const userId = req.userId;
        const body = req.body;

        const order = new Order({
            user: userId,
            orderlines: body.orderLines,
            submitDate: new Date()
        });

        order.save()
            .then(() => {
                res.status(200).json({result: order})
            })
            .catch((err) => {
                next({
                    code: 404,
                    message: "Something went wrong while saving the order"
                })
            })
    },

    updateOrder: (req, res, next) => {
        const body = req.body;
        const id = req.params.id;

        Order.update({_id: id}, body)
            .then(() => Order.findById(id))
            .then(o => {
                res.status(200).json({result: o})
            })
            .catch(err => {
                next({
                    code: 404,
                    message: "Something went wrong when updating order"
                })
            })
    },

    deleteOrder: (req, res, next) => {
        const id = req.params.id;

        Order.findOneAndDelete({_id: id})
            .then((order) => {
                res.status(200).json({result: order})
            })
            .catch((err) => {
                next({
                    code: 404,
                    message: "Something went wrong while delete an order"
                })
            })
    },

    getOrdersByUsername: (req, res, next) => {
        const username = req.params.username;

        Order.find({})
            .populate('user')
            .then((orders) => {
                res.status(200).json({result: orders.filter((o) => {
                        return o.user.username === username
                    })})
            })
            .catch((err) => {
                next({
                    code: 404,
                    message: "Something went wrong getting the orders by username"
                })
            })
    }
};
