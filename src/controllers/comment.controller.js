const Comment = require('../models/comment.model');

module.exports = {
    getAllComments: (req, res, next) => {
        Comment.find({})
            .then((comments) => {
                res.status(200).json({result: comments});
            });
    },

    getCommentById: (req, res, next) => {
        const id = req.params.id;
        Comment.findById(id)
            .then((c) => {
                res.status(200).json({result: c});
            });
    },

    postComment: (req, res, next) => {
        const userId = req.userId;
        const beer = req.body.beer;
        const content = req.body.content;
        const rating = req.body.rating;

        let comment = new Comment({
            author: userId,
            beer: beer,
            content: content,
            rating: rating,
            submitDate: new Date().getDate()
        });

        comment.save()
            .then(() => Comment.findOne({author: userId, beer: beer, content: content}))
            .then((c) => {
                res.status(200).json({result: c});
            });
    },

    updateComment: (req, res, next) => {
        const id = req.params.id;
        req.body.lastUpdateDate = new Date();
        Comment.update({_id: id}, req.body)
            .then(() => Comment.findOne({_id: id}))
            .then((c) => {
                res.status(200).json({result: c});
            });
    },
}
